<?php

namespace App\Http\Controllers;

use App\Client;
use App\Resource;
use Illuminate\Http\Request;

use Mail;
use App\Mail\reminder;

class PagesController extends Controller
{
    public function index()
    {	
    	$resources = Resource::get();
    	$client = '';
    	return view('welcome')->with(compact([
    		'resources','client'
    	]));
    }

    public function sendnew()
    {

         $data = ['one' => 'firstdata' , 'two' => 'seconddata'];
         $user['to']='jiivakarthick84@gmail.com';
         $test = Mail::send('mail', $data,function($messages) use ($user){
         $messages->to($user['to']);
         $messages->subject('hello machi');
             });
            return $test;
    
    }


    public function orgIndex($clientSlug)
    {
        
        $resources = Resource::get();
        $client = Client::where('slug',$clientSlug)->firstOrFail();
        // return $client;
    	return view('welcome',compact(['resources','client']));
    }

      public function sendEmails(Request $request)
    {
        // return $request->all();
        if($request->isMethod('post'))
        {
            $res_ids = array_map('intval', explode(',', $request->res_ids));
            
            if($this->arrayHasOnlyInts($res_ids))
            {
            $emails = explode(',', $request->toEmail);
            $resources = Resource::whereIn('id',$res_ids)->get();
            $emailBody = '';
            
            foreach($resources as $res)
            {
                $url = '';
                if($res->separate_pi == "1")
                {
                    $pi_pdf = json_decode($res->pdf_with_pi);
                    $url = asset('storage/'.$pi_pdf[0]->download_link);
                }
                elseif($res->cms_link)
                {
                    $url = $res->cms_link;
                }
                elseif($res->video_link)
                {
                    $url = $res->video_link;
                }
                elseif($res->pdf != "[]")
                {
                    // return dd($res->pdf);
                    $pdf = json_decode($res->pdf);
                    // return dd($pdf);
                    if($pdf[0]->download_link)
                    {
                    $url = asset('storage/'.$pdf[0]->download_link);
                    }
                   
                }
                
                
    
                $emailBody .= '<b>Here is a resource that might be of interest. Please open the link to view:<br><br>'.$res->title.'</b><br>'.$res->english_description.'<br> <a href='.$url.'>'.$url.'</a> <br><br>';
            }
            
            
            // $emailBody = $request->emailBody;
            $data = array( 'emailBody'=> $emailBody );
            // ptint_r($res_ids);
            //      exit;
             $count=0;
              if($data)
              {
                  $count++;
              }
             
             if($count==1)
              {
                //  print_r(count($res_id));exit;
                  if(count($res_ids) <= 1)
                  {
                    $checkMail = Mail::send('emails.sharedResource', $data, function($message) use ($emails)
                    {    
                        // print_r($emails);
                        // exit;
                        
                        $message->from('application@hasotech.com');
                        $message->to($emails)->subject('A resource has been shared with you');    
                        // }
                        // else
                        // {
                        //       $message->from('support@liverlifepro.com');
                        // $message->to($emails)->subject('single resources has been shared with you');    
                        // }
                        
                    });
                  }
                  else
                  {
                      $checkMail = Mail::send('emails.sharedResource', $data, function($message) use ($emails)
                    {    
                        // if( $emailBody->cms_link >1)
                        // {
                        
                        $message->from('application@hasotech.com');
                        $message->to($emails)->subject('Multiple resources have been shared with you');    
                        // }
                        // else
                        // {
                        //       $message->from('support@liverlifepro.com');
                        // $message->to($emails)->subject('single resources has been shared with you');    
                        // }
                        
                    });
                  }
              }
    
            // if($checkMail){
                return redirect()->back()->with('emailSuccess','Email Sent Successfully.!');
            }
            else
            {
                 return redirect()->back()->with('emailError','Something Went Wrong.');
            }
        }
        else
            { return abort(404); }
    }
     public function viewPdf()
    {
        return response()->file(public_path('Hizentra 8.5 X 11 PI.pdf'));       
    }
    
    function arrayHasOnlyInts($array)
    {
        foreach ($array as $value)
        {
            if (!is_int($value)) // there are several ways to do this
            {
                 return false;
            }
            
        }
        return true;
    }
    
    
}
