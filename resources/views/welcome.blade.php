@section('title')
Skytrofaqsa – QSA – PLUGIN
@endsection
@section('css')
@endsection
@extends('layouts.layout')
@section('content')
<!-- Main Section -->
<form class="form" method="POST" action="{{route('pdfMerge')}}" target="_blank|_self|_parent|_top|framename"> 
    @csrf



    <section class="side-space main-section">
        <section class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="top-link ">
                        <ul>
                            <li class = "pglink"><a href="#provider">Provider</a></li>
                            <li class = "pglink"><a href="#patients">Patient</a></li>
                            
                            <li><a href="https://ascendispharma.us/products/pi/skytrofa/skytrofa_pi.pdf" target="_blank" class="gaClick" data-label="PI">Prescribing Information</a></li>
                          
                            <li><a href="#isi"  class="gaClick" data-scroll data-label="ISI">Important Safety Information</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-12">
                    <nav class="notification">
                        <ul>
                            <li><img src="{{asset('img/imgonlin.jpg')}}"> Select tool(s)</li>
                            <li><img class="scr" id="scr5" src="{{asset('img/Screenshot_5.png')}}"> Preview tool</li>
                            <li><img id="inedown" src="{{asset('img/Screenshot_1.png')}}"> Description</li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="row headings " id="provider">
                <div class="col-6">
                    <div class="mr-left fsdfs">
                        <!--<h3>PROVIDER</h3>-->
                    </div>
                </div>
                {{-- <div class="col-6">
                    <small>*These resources cannot be printed.</small>
                </div> --}}
            </div>
           <!--  <div class="row">
                <div class="col-12  mr-left">
                    <div class="pd-left sub-title">
                        <h3>Resources</h3>
                    </div>
                </div>
            </div> -->
            {{-- Provider pdf --}}
            <div class="row ">
                <div class="col-12 ">
                    <div class="pd-left">
                        <div class="acc pd-left">
                             <h3 class="fys">PROVIDER</h3>
                            @foreach( $resources as $resource )
                                @if( $resource->menu_type == 1 && $resource->category_type == 1 )
                                    @php
                                       
                                        if($resource->separate_pi == "1")
                                        {
                                            $pi_pdf = json_decode($resource->pdf_with_pi);
                                        }
                                        else
                                        {
                                             $pdf = json_decode($resource->pdf);
                                        }
                                    @endphp
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="providerResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}" 
                                                data-title="{{$resource->title}}"
                                                data-english_description="{{$resource->english_description}}"
                                                data-spanish_description="{{$resource->spanish_description}}"
                                               
                                                    @if($resource->separate_pi == 1)
                                                        data-tool-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                        data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                        data-title="{{$resource->title}}"
                                                        data-can-print="{{$resource->can_print}}"
                                                   @elseif(!empty($pdf[0]->download_link))
                                                        data-tool-url="{{$resource->cms_link}}"
                                                        data-print-url="{{$resource->cms_link}}"
                                                        data-title="{{$resource->title}}"
                                                        data-can-print="{{$resource->can_print}}"
                                                    @endif
                                               
                                                >
                                            
                                            
                                                <label for="{{$resource->id}}">{!!$resource->title!!}</label>
                                                    
                                            </span>
                                            @if($resource->cms_link)
                                                <a href="{{$resource->cms_link}}" target="_blank" class="preview">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                </a>
                                            @else
                                                @if($resource->separate_pi == 1)
                                                     <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                @elseif(!empty($pdf[0]->download_link))
                                                    <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                @endif
                                            @endif
                                            <a class="drop-down">
                                                <img src="{{asset('img/Screenshot_1.png')}}">
                                            </a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                           {{$resource->english_description}}
                                           @if($resource->can_print == 0)
                                            @if($resource->no_print_title == 1)
                                               <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                            @endif
                                           @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>





            {{-- provider fillable forms --}}
            <!-- <div class="row mr-top1">
                <div class="col-12  mr-left">
                    <div class="pd-left sub-title">
                        <h3>Fillable Forms*</h3>

                        <aside class="points ">
                            <ul class="">
                                <li><span>1</span>Fill in<br>
                                    form</li>
                                <li><span>2</span>Print and/or<br>
                                    save/submit</li>
                                <li><span>3</span>You can email<br>
                                    or copy the link</li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div> -->
              
            

            <div class="row ">
                <div class="col-12 ">
                    <div class="pd-left">
                        <div class="acc pd-left">
                            <div class="fil">Fillable Forms</div>
                             @foreach( $resources as $resource )
                                @if( $resource->menu_type == 1 && $resource->category_type == 2)
                                
                                
                                    @php
                                       
                                        if($resource->separate_pi == "1")
                                        {
                                            $pi_pdf = json_decode($resource->pdf_with_pi);
                                        }
                                       
                                    @endphp
                                    
                                    <div class="acc__card">
                                         
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="providerFillableForms[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                data-english_description="{{$resource->english_description}}"
                                                data-spanish_description="{{$resource->spanish_description}}"
                                                 data-title="{{$resource->title}}"
                                                 @if($resource->separate_pi == 0)
                                                   
                                                    data-tool-url="{{$resource->cms_link}}"
                                                    data-print-url="{{$resource->cms_link}}"
                                                   
                                                    data-can-print="{{$resource->can_print}}"
                                                @elseif(!empty($pdf[0]->download_link) )
                                                    data-tool-url="{{$resource->cms_link}}"
                                                    data-print-url="{{$resource->cms_link}}"
                                                   
                                                    data-can-print="{{$resource->can_print}}"
                                                @elseif($resource->cms_link)
                                                    
                                                @endif
                                                >
                                                <label for="{{$resource->id}}">{!!$resource->title!!}</label>
                                            </span>
                                            @if($resource->separate_pi == 1)
                                                <a href="" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                    <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                </a>
                                            @elseif($resource->cms_link)
                                                <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                </a>
                                            @else
                                             @if(!empty($pdf[0]->download_link))
                                                <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                    <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                </a>
                                             @endif
                                            @endif
                                            <a class="drop-down"><img src="{{asset('img/Screenshot_1.png')}}"></a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                           {{$resource->english_description}}
                                           @if($resource->can_print == 0)
                                            @if($resource->no_print_title == 1)
                                               <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                            @endif
                                           @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            
            @foreach( $resources as $resource )
                @if($resource->menu_type == 1 && $resource->category_type == 3)
                 <div class="row mr-top1">
                        <div class="col-12  mr-left">
                            <div class="pd-left sub-title">
                                <h3>Video</h3>

                                <aside class="points ">
                                    <ul class="">
                                        <li><span>1</span>Viewed<br>
                                            online</li>
                                        <li><span>2</span>You can email<br>
                                            or copy the link</li>
                                    </ul>
                                </aside>

                            </div>
                        </div>
                    </div>

                  
               
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    @foreach( $resources as $resource )
                                        @if( $resource->menu_type == 1 && $resource->category_type == 3 )
                                            <div class="acc__card">
                                                <div class="acc__title">
                                                    <span class="form-group">
                                                        <input type="checkbox" name="VideoResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                        data-can-print="{{$resource->can_print}}" data-title="{{$resource->title}}"  @if(!empty($resource->video_link))
                                                        data-tool-url="{{$resource->video_link}}"
                                                        
                                                         @elseif($resource->redirect_to_cms == 1)
                                                         data-tool-url="{{$resource->cms_link}}"
                                                         @endif
                                                        >
                                                        <label for="{{$resource->id}}">{{$resource->title}}
                                                        @if($resource->can_print == 0)
                                                            @if($resource->no_print_title == 1)
                                                                <small class="small-info">PLEASE NOTE: This is a video. It
                                                                    cannot be printed.</small>
                                                            @endif
                                                        @endif
                                                        </label>
                                                    </span>
                                                    @if($resource->redirect_to_cms == 1)
                                                        <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                        </a>
                                                    @else
                                                    @if(!empty($resource->video_link))
                                                        <a href="{{$resource->video_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                        </a>
                                                     @endif
                                                    @endif
                                                    {{-- <a class="preview"><img id="scr5" src="{{asset('img/Screenshot_5.png')}}"></a> --}}
                                                    <a class="drop-down"><img src="{{asset('img/Screenshot_1.png')}}"></a>
                                                </div>
                                                <div class="acc__panel  mr-left">
                                                    {{$resource->english_description}}
                                                    @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                            <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
            {{-- video ends here --}}
        </section>
    </section>


   

    <!--  <section class=" mr-top1" id='caregiver'>
            <section class="container">
                <div class="row justify-content-between align-items-center ">
                    <div class="col-12">
                        <div class="laguage-sel">
                            <p>Identify a caregiver whenever possible who will help the patient with his or her
                                    treatment plan.</p>
                            {{-- <p>Choose the language of the following caregiver and patient resources.</p>
                            <ul class="tabs">
                                <li class="active" data-tab="tab-1"> English </li>
                                <li class=" " data-tab="tab-2"> Spanish </li>
                            </ul> --}}

                        </div>
                    </div>
                </div>
            </section>
        </section> -->


                          <section class="container bg-grey">
                         <div class="education-grey">
                            Educational resources for patients and their caregivers about
                      <span class="chronic-txt" >managing Pediatric Growth Hormone Deficiency with SKYTROFA.</span>
                        </div>
                         </section>

    <section class="side-space main-section mr-top2 ">
        <section class="container">
            <div id="tab-1" class="tab-content current">
                <div class="row headings " id="patients">
                    <div class="col-md-8">
                        <div class="mr-left gfdf" id="dfa">
                            <!--<h3>PATIENT</h3>-->
                        </div>
                    </div>
                </div>
               <!--  <div class="row">
                    <div class="col-12  mr-left">
                        <div class="pd-left sub-title">
                            <h3>Resources</h3>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12 ">
                        <div class="pd-left">
                            <div class="acc pd-left">
                                 <h3 class="fys1">PATIENT</h3>
                                @foreach( $resources as $resource )
                                    @if( $resource->menu_type == 2 && $resource->category_type == 1 )
                                        @php
                                        if($resource->separate_pi == 1)
                                        {

                                            $pi_pdf = json_decode($resource->pdf_with_pi);
                                        }
                                        else
                                        {
                                            $pdf = json_decode($resource->pdf);
                                        }
                                        @endphp
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="CPResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                    data-english_description="{{$resource->english_description}}"
                                                    data-spanish_description="{{$resource->spanish_description}}"
                                                     data-title="{{$resource->title}}"
                                                    @if($resource->separate_pi == 1)
                                                        data-tool-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                        data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                       
                                                        data-can-print="{{$resource->can_print}}"
                                                    @elseif(!empty($pdf[0]->download_link))
                                                        data-tool-url="{{$resource->cms_link}}"
                                                        data-print-url="{{$resource->cms_link}}"
                                                       
                                                        data-can-print="{{$resource->can_print}}"
                                                    @elseif($resource->cms_link)
                                                        data-tool-url = "{{$resource->cms_link}}"
                                                        data-print-url = "{{$resource->cms_link}}"
                                                        data-can-print="{{$resource->can_print}}"
                                                    @endif
                                                    >
                                                    <label for="{{$resource->id}}">{!!$resource->title!!}
                                                    </label>
                                                    @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                          <!--   <small class="small-info">PLEASE NOTE: This is an online form
                                                            that allows you to download the card.</small> -->
                                                        @endif
                                                    @endif
                                                </span>
                                                @if($resource->cms_link)
                                                    <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                @else
                                                @if($resource->separate_pi ==1)
                                                    <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                 @elseif(!empty($pdf[0]->download_link))
                                                    <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                 @endif
                                                @endif
                                                {{-- <a class="preview"><img id="scr5" src="{{asset('img/Screenshot_5.png')}}"></a> --}}
                                                <a class="drop-down"><img src="{{asset('img/Screenshot_1.png')}}"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                {{$resource->english_description}}
                                                <!--@if($resource->can_print == 0)-->
                                                <!--    @if($resource->no_print_title == 1)-->
                                                <!--        <span class="note">PLEASE NOTE: This tool can not be printed.</span>-->
                                                <!--    @endif-->
                                                <!--@endif-->
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                 {{-- fillable forms --}}
                @foreach( $resources as $resource )
                    @if($resource->menu_type == 2 && $resource->category_type == "2" )
                        <div class="row mr-top1">
                            <div class="col-12  mr-left">
                                <div class="pd-left sub-title">
                                    <h3>Fillable Forms*</h3>

                                    <aside class="points ">
                                        <ul class="">
                                            <li><span>1</span>Fill in <br>
                                                form</li>
                                            <li><span>2</span>Print and/or<br>
                                                save/submit</li>
                                            <li><span>3</span>You can email<br>
                                                or copy the link</li>
                                        </ul>
                                    </aside>
                                </div>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-12 ">
                                <div class="pd-left">
                                    <div class="acc pd-left">
                                        @foreach( $resources as $resource )
                                            @if( $resource->menu_type == 2 && $resource->category_type == 2 )
                                                @php
                                                if($resource->separate_pi==1)
                                                {
                                                    $pi_pdf = json_decode($resource->pdf_with_pi);
                                                }
                                                else
                                                {

                                                    $pdf = json_decode($resource->pdf);
                                                }
                                                @endphp
                                                <div class="acc__card">
                                                    <div class="acc__title">
                                                        <span class="form-group">
                                                            <input type="checkbox" name="providerFillableForms[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                            data-english_description="{{$resource->english_description}}"
                                                            data-spanish_description="{{$resource->spanish_description}}"
                                                            @if($resource->separate_pi == 1)
                                                                data-tool-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                                data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                                data-title="{{$resource->title}}"
                                                                data-can-print="{{$resource->can_print}}"
                                                            @elseif(!empty($pdf[0]->download_link))
                                                                data-tool-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                                data-print-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                                data-title="{{$resource->title}}"
                                                                data-can-print="{{$resource->can_print}}"
                                                            @endif
                                                            >
                                                            <label for="{{$resource->id}}">{{$resource->title}}*</label>
                                                        </span>
                                                        @if($resource->redirect_to_cms == 1)
                                                            <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                    <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                            </a>
                                                        @else
                                                         @if($resource->separate_pi == 1)
                                                            <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                            </a>
                                                         @elseif(!empty($pdf[0]->download_link))
                                                            <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                            </a>
                                                         @endif
                                                        @endif
                                                        <a class="drop-down"><img src="{{asset('img/Screenshot_1.png')}}"></a>
                                                    </div>
                                                    <div class="acc__panel  mr-left">
                                                       {{$resource->english_description}}
                                                       @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                           <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                                        @endif
                                                       @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                
                {{-- english video starts here  --}}
              <!--   @foreach( $resources as $resource )
                 @if( $resource->menu_type == 2 && $resource->category_type == 3 )
                    <div class="row mr-top1">
                        <div class="col-12  mr-left">
                            <div class="pd-left sub-title">
                                <h3>Video</h3>

                                <aside class="points ">
                                    <ul class="">
                                        <li><span>1</span>Viewed<br>
                                            online</li>
                                        <li><span>2</span>You can email<br>
                                            or copy the link</li>
                                    </ul>
                                </aside>

                            </div>
                        </div>
                    </div>
                @endif
                @endforeach -->
                <div class="row ">
                    <div class="col-12 ">
                        <div class="pd-left">
                            <div class="acc pd-left">
                                @foreach( $resources as $resource )
                                    @if( $resource->menu_type == 2 && $resource->category_type == 3 )
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="VideoResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                    data-can-print="{{$resource->can_print}}"  
                                                    data-title="{{$resource->title}}"
                                                    data-english_description="{{$resource->english_description}}"
                                                     data-print-url="{{ $resource->video_link }}"  @if(!empty($resource->video_link))
                                                        data-tool-url="{{$resource->video_link}}"
                                                        
                                                         @elseif($resource->redirect_to_cms == 1)
                                                         data-tool-url="{{$resource->cms_link}}"
                                                         @endif
                                                    >

                                                 <!--    <label for="{{$resource->id}}">{{$resource->title}}
                                                    @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                            <small class="small-info">PLEASE NOTE: This is a video. It
                                                                    cannot be printed.</small>
                                                        @endif
                                                    @endif
                                                    </label> -->
                                                </span>
                                                 @if($resource->cms_link == 1)
                                                    <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                @else
                                               @if(!empty($resource->video_link))
                                                    <a href="{{$resource->video_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                 @endif
                                                @endif
                                                <!--<a class="preview"><img src="{{asset('img/search.png')}}"></a>-->
                                                <a class="drop-down"><img src="{{asset('img/Screenshot_1.png')}}"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                {{$resource->english_description}}
                                                <!--@if($resource->can_print == 0)-->
                                                <!--     @if($resource->no_print_title == 1)-->
                                                <!--        <span class="note"> PLEASE NOTE: This tool can not be printed.</span>-->
                                                <!--    @endif-->
                                                <!--@endif-->
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                {{-- english video ends here  --}}
               
            </div>
            
            {{-- spanish tab starts here --}}
            <div id="tab-2" class="tab-content ">
                <div class="row headings ">
                    <div class="col-md-8">
                        <div class="mr-left">
                            <h3>Caregiver and Patient</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12  mr-left">
                        <div class="pd-left sub-title">
                            <h3>Resources</h3>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-12 ">
                        <div class="pd-left">
                            <div class="acc pd-left">
                                @foreach( $resources as $resource )
                                    @if( $resource->menu_type == 2 && $resource->category_type == 1 )
                                        @php
                                            $pdf = json_decode($resource->pdf);
                                        @endphp
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="CPSpanishResources[]" class="checkbox gacheckbox" id="spanish{{$resource->id}}" value="{{$resource->id}}"
                                                    data-english_description="{{$resource->english_description}}"
                                                    data-spanish_description="{{$resource->spanish_description}}"
                                                    @if(!empty($pdf[0]->download_link))
                                                    data-tool-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                    data-print-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                    data-title="{{$resource->title}}"
                                                    data-can-print="{{$resource->can_print}}"
                                                    @endif
                                                    >
                                                    <label for="spanish{{$resource->id}}">
                                                    {{$resource->title}}
                                                    @if($resource->can_print == 0)
                                                     @if($resource->no_print_title == 1)
                                                         <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                                     @endif
                                                    @endif
                                                    </label>
                                                </span>
                                                 @if($resource->cms_link == 1)
                                                    <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                @else
                                                 @if(!empty($pdf[0]->download_link))
                                                    <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img id="scr5" src="{{asset('img/Screenshot_5.png')}}">
                                                    </a>
                                                 @endif
                                                @endif
                                                {{-- <a class="preview"><img id="scr5" src="{{asset('img/Screenshot_5.png')}}"></a>  --}}
                                                <a class="drop-down"><img src="{{asset('img/Screenshot_1.png')}}"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                {{$resource->english_description}}
                                                <!--@if($resource->can_print == 0)-->
                                                <!--     @if($resource->no_print_title == 1)-->
                                                <!--        <span class="note">PLEASE NOTE: This tool can not be printed.</span>-->
                                                <!--    @endif-->
                                                <!--@endif-->
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{-- spanish tab ends here --}}
        </section>






            <section class="bg-c">
        <a class="back-to-top">
            Back to Top <img src="{{asset('img/imagetop.png')}}" class="bt">
        </a>
        
        <section class="container">
            <div class="row ">
                <div class="col-12 d-flex justify-content-center">
                    <a href="#" data-toggle="modal" data-target="#myModal" class="button gaEmail" id="email_res">Email</a>
                    <button class="button viewPrint" >View/Print</button>
                    <button class="button gaCopy" id="copy-tool">Copy Link</button>
                </div>
            </div>
        </section>
    </section>
      



                  <section class="para">
                    <div class="row ">
                        <div class="col-12 imp-safety">
                            <h6 id="" class="head">INDICATION</h6>
                            <div class="hiz">SKYTROFA<sup>®</sup> is a human growth hormone indicated for the treatment of pediatric patients 1 year and older 
                                 who weigh at least 11.5 kg and have growth failure due to inadequate secretion of endogenous growth 
                             hormone (GH).</div>
                            <h6 id="isi" class="head">IMPORTANT SAFETY INFORMATION</h6>
                            <ul>
                       <li class="crits">
                          SKYTROFA is contraindicated in patients with:
                          <ul class="crits">
                            <li class="il-es">
                              Acute critical illness after open heart surgery, abdominal surgery or multiple accidental trauma, or if 
                        you have acute respiratory failure due to the risk of increased mortality with use of armacologic 
                         doses of somatropin.
                           </li>
                           <li class="crits">
                               Hypersensitivity to somatropin or any of the excipients in SKYTROFA. Systemic hypersensitivity 
                               reactions have been reported with post-marketing use of somatropin products.
                           </li>
                           <li class="crits">
                               Closed epiphyses for growth promotion.
                           </li>
                           <li class="crits">
                               Active malignancy.
                           </li>
                           <li class="crits">
                               Active proliferative or severe non-proliferative diabetic retinopathy.
                           </li>
                           <li class="crits">
                               Prader-Willi syndrome who are severely obese, have a history of upper airway obstruction or 
                                 sleepapnea or have severe respiratory impairment due to the risk of sudden death.
                           </li>
                          </ul>
                       </li>
                       </ul>
    <ul>
        <li class="crits">
             Increased mortality in patients with acute critical illness due to complications following open heart 
surgery, abdominal surgery or multiple accidental trauma, or those with acute respiratory failure has 
been reported after treatment with pharmacologic doses of somatropin. Safety of continuing SKYTROFA 
treatment in patients receiving replacement doses for the approved indication who concurrently develop 
these illnesses has not been established.
        </li>
        <li class="crits">
            Serious systemic hypersensitivity reactions including anaphylactic reactions and angioedema have 
been reported with post-marketing use of somatropin products. Do not use SKYTROFA in patients with 
known hypersensitivity to somatropin or any of the excipients in SKYTROFA.
        </li>
        <li  class="crits">
            There is an increased risk of malignancy progression with somatropin treatment in patients with active 
malignancy. Preexisting malignancy should be inactive with treatment completed prior to starting 
SKYTROFA. Discontinue SKYTROFA if there is evidence of recurrent activity.
        </li>
        <li  class="crits">
            In childhood cancer survivors who were treated with radiation to the brain/head for their first neoplasm 
and who developed subsequent growth hormone deficiency (GHD) and were treated with somatropin, an 
increased risk of a second neoplasm has been reported. Intracranial tumors, in particular meningiomas, 
were the most common of these second neoplasms. Monitor all patients with a history of GHD 
secondary to an intracranial neoplasm routinely while on somatropin therapy for progression or 
recurrence of the tumor.
        </li>

        <li  class="crits">
             Because children with certain rare genetic causes of short stature have an increased risk of developing 
malignancies, practitioners should thoroughly consider the risks and benefits of starting somatropin in 
these patients. If treatment with somatropin is initiated, carefully monitor these patients for 
development of neoplasms. Monitor patients on somatropin therapy carefully for increased growth, or 
potential malignant changes of preexisting nevi. Advise patients/caregivers to report marked changes in 
behavior, onset of headaches, vision disturbances and/or changes in skin pigmentation or changes in 
the appearance of preexisting nevi.
        </li>
        <li  class="crits">
            Treatment with somatropin may decrease insulin sensitivity, particularly at higher doses. New onset 
type 2 diabetes mellitus has been reported in patients taking somatropin. Undiagnosed impaired 
glucose tolerance and overt diabetes mellitus may be unmasked. Monitor glucose levels periodically in 
all patients receiving SKYTROFA. Adjust the doses of antihyperglycemic drugs as needed when 
SKYTROFA is initiated in patients.
        </li>
        <li class="crits">
             Intracranial hypertension (IH) with papilledema, visual changes, headache, nausea, and/or vomiting has 
been reported in a small number of patients treated with somatropin. Symptoms usually occurred within 
the first 8 weeks after the initiation of somatropin and resolved rapidly after cessation or reduction in 
dose in all reported cases. Fundoscopic exam should be performed before initiation of therapy and 
periodically thereafter. If somatropin-induced IH is diagnosed, restart treatment with SKYTROFA at a 
lower dose after IH-associated signs and symptoms have resolved.
        </li>
        <li  class="crits">
            Fluid retention during somatropin therapy may occur and is usually transient and dose dependent.
        </li>
        <li  class="crits">
             Patients receiving somatropin therapy who have or are at risk for pituitary hormone deficiency(s) may be 
at risk for reduced serum cortisol levels and/or unmasking of central (secondary) hypoadrenalism. 
Patients treated with glucocorticoid replacement for previously diagnosed hypoadrenalism may require 
an increase in their maintenance or stress doses following initiation of SKYTROFA therapy. Monitor 
patients for reduced serum cortisol levels and/or need for glucocorticoid dose increases in those with 
known hypoadrenalism.
        </li>
        <li  class="crits">
            Undiagnosed or untreated hypothyroidism may prevent response to SKYTROFA. In patients with GHD, 
central (secondary) hypothyroidism may first become evident or worsen during SKYTROFA treatment. 
Perform thyroid function tests periodically and consider thyroid hormone replacement.
        </li>
        <li  class="crits">
            Slipped capital femoral epiphysis may occur more frequently in patients undergoing rapid growth. 
Evaluate pediatric patients with the onset of a limp or complaints of persistent hip or knee pain.
        </li>
        <li  class="crits">
            Somatropin increases the growth rate and progression of existing scoliosis can occur in patients who 
experience rapid growth. Somatropin has not been shown to increase the occurrence of scoliosis. 
Monitor patients with a history of scoliosis for disease progression. 
        </li>
        <li  class="crits">
            Cases of pancreatitis have been reported in pediatric patients receiving somatropin. The risk may be 
greater in pediatric patients compared with adults. Consider pancreatitis in patients who develop 
persistent severe abdominal pain
        </li>
        <li  class="crits">
            When SKYTROFA is administered subcutaneously at the same site over a long period of time, 
lipoatrophy may result. Rotate injection sites when administering SKYTROFA to reduce this risk.
        </li>
        <li  class="crits">
            There have been reports of fatalities after initiating therapy with somatropin in pediatric patients with 
Prader-Willi syndrome who had one or more of the following risk factors: severe obesity, history of upper 
airway obstruction or sleep apnea, or unidentified respiratory infection. Male patients with one or more 
of these factors may be at greater risk than females. SKYTROFA is not indicated for the treatment of 
pediatric patients who have growth failure due to genetically confirmed Prader-Willi syndrome.
        </li>
        <li  class="crits">
            Serum levels of inorganic phosphorus, alkaline phosphatase, and parathyroid hormone may increase 
after somatropin treatment.
        </li>
        <li  class="crits">
            The most common adverse reactions (≥ 5%) in patients treated with SKYTROFA were: viral infection 
(15%), pyrexia (15%), cough (11%), nausea and vomiting (11%), hemorrhage (7%), diarrhea (6%), 
abdominal pain (6%), and arthralgia and arthritis (6%).
        </li>
        <li  class="crits">
            SKYTROFA can interact with the following drugs:
        </li>
        <ul  class="crits">
        <li class="crits">
            Glucocorticoids: SKYTROFA may reduce serum cortisol concentrations which may require an 
increase in the dose of glucocorticoids.
        </li>
        <li class="crits">
            Oral Estrogen: Oral estrogens may reduce the response to SKYTROFA. Higher doses of SKYTROFA 
may be required.
        </li>
        <li class="crits">
            Insulin and/or Other Hypoglycemic Agents: SKYTROFA may decrease insulin sensitivity. Patients 
with diabetes mellitus may require adjustment of insulin or hypoglycemic agents.
        </li>
        <li class="crits">
            Cytochrome P450-Metabolized Drugs: Somatropin may increase cytochrome P450 
(CYP450)-mediated antipyrine clearance. Carefully monitor patients using drugs metabolized by 
CYP450 liver enzymes in combination with SKYTROFA.
        </li>
        </ul>
        
            
        </li>
    </ul>
  

<div class="ple">You are encouraged to report side effects to FDA at (800) FDA-1088 or <a class="pre" target="_blank" href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program">www.fda.gov/medwatch</a>. You may 
also report side effects to Ascendis Pharma at 1-844-442-7236.</div>

        <div class="ple">Please 
 <a target="_blank" class="med" href="https://ascendispharma.us/products/pi/skytrofa/skytrofa_pi.pdf">click here</a> for full Prescribing Information for SKYTROFA. </div>


















</form>
<!-- Main Section Over-->

@endsection
@section('js')
<script type="text/javascript">
    function errorFunction(message) {
        event.preventDefault();
        $('#errorResponse').html(message);
        $('#errorResponse').fadeIn();
        setTimeout(function () { $('#errorResponse').fadeOut(); }, 3000);
    }
    var canPrintCheck = [];
    var cannotPrintCheck = [];
    var urls;
    var titles = [];
    var resource_ids = [];
    $(document).on('click', '.checkbox', function(event) {
    
        if (($(this).attr('data-can-print') == 1)) {
            if($(this).is(':checked')){
                console.log( ($(this).val()  )  );
                canPrintCheck.push($(this).val());    
            }else{
                var index = canPrintCheck.indexOf($(this).val());
                // if (index !== -1) canPrintCheck.splice(index, 1);  
                // this is used to reason for page expired 409 error 
                // pwr changes
            }
            
        }else{
             if($(this).is(':checked')){
                cannotPrintCheck.push($(this).val());    
            }else{
                var index = cannotPrintCheck.indexOf($(this).val());
                if (index !== -1) cannotPrintCheck.splice(index, 1);
            }
        }
       
        urls = ($(':checkbox:checked').map(function() {
             return $(this).attr('data-print-url');
           }).get());
        
        titles = ($(':checkbox:checked').map(function() {
             return $(this).attr('data-title') + ' ';
           }).get());
           
            if($(this).is(':checked'))
            {   resource_ids.push($(this).attr('id'));
                console.log($(this).attr('data-title'));
                var emailBody = '<div id="eb-'+$(this).attr('id')+'" class = "ebclass"><b class="makeme-ld">Here is a resource that might be of interest. Please open the link to view:<br><br>'+$(this).attr('data-title')+'</b><br>'+$(this).attr('data-english_description')+'<br> <a href="'+$(this).attr('data-print-url')+'">'+$(this).attr('data-print-url')+'</a> <br><br></div>';
                $('#email-body').append(emailBody);
                
            }
            else
            {
                var removeItem = $(this).attr('id');
                //  jQuery.splice(index,1);
                
                resource_ids = jQuery.grep(resource_ids, function(value) {
                  return value != removeItem;
                });
                
                $('#eb-'+$(this).attr('id')).remove();
               
            }

        $('#email-body-hidden').val($('#email-body').html());
                
        var count = $('#email-body .ebclass').length ; 
        if(count == 1)
        {
            $('#ebres1').show();
            $('#ebresmul').hide(); 
        }
        else if(count >= 2)
        {
           $('#ebres1').hide();
            $('#ebresmul').show(); 
        }

    });
    
    $(document).on('click', '.viewPrint', function(event) {
        console.log(canPrintCheck.length);
        if(cannotPrintCheck.length === 0){
            if (canPrintCheck.length === 0) {
               
            errorFunction('Please select at least 1 printable tool')

            }else{
              
                urls = ($(':checkbox:checked').map(function() {
                    canPrintCheck.push(($(this).attr('data-can-print')));
                   }).get());
                   console.log(urls);
                //   alert(urls);
                //   exit;
                if($.inArray("0", canPrintCheck) !== -1){
                    errorFunction('Please unselect the link which cannot be printed');
                }
            }
        }else{
            errorFunction('Please uncheck non-printable tool');
        }
           
    });
    
    var checkBoxCategory = @if(empty($client)) 'plugin' @else '{{$client->name}}' @endif ;
    // $('.gacheckbox').click(function(e){
        
    //     if($(this).is(':checked')){
    //         console.log(1)
    //         var label = $(this).attr('data-title');
    //         var test = ga('send', {
    //           hitType: 'event',
    //           eventCategory: checkBoxCategory,
    //           eventAction: 'Checked',
    //           eventLabel: label
    //         });
    //     }
    // });
    
    // $('.gapreview').click(function(e){
    //         console.log(1)
    //         var label = $(this).attr('data-title');
    //         var test = ga('send', {
    //           hitType: 'event',
    //           eventCategory: checkBoxCategory,
    //           eventAction: 'View',
    //           eventLabel: label
    //         });
    // });
    
    // $(document).on('click', '.viewPrint', function(event) {
    //     console.log("view/print");
    //     // console.log(titles);
    //         var label = titles;
    //         var test = ga('send', {
    //           hitType: 'event',
    //           eventCategory: checkBoxCategory,
    //           eventAction: 'Print',
    //           eventLabel: label
    //         });
        
    // });
    
    // $(document).on('click', '.gaEmail', function(event) {
    //     // console.log(titles);
    //         var label = titles;
    //         var test = ga('send', {
    //           hitType: 'event',
    //           eventCategory: checkBoxCategory,
    //           eventAction: 'Email',
    //           eventLabel: label
    //         });
        
    // });
    
    // $(document).on('click', '.gaCopy', function(event) {
    //     console.log(titles);
    //         var label = titles;
    //         var test = ga('send', {
    //           hitType: 'event',
    //           eventCategory: checkBoxCategory,
    //           eventAction: 'Copy',
    //           eventLabel: label
    //         });
        
    // });
    
    // $('.gaClick').click(function(e){
    //         // google analytics click functionality
    //       var label = $(this).attr('data-label');
    //       var test = ga('send', {
    //          hitType: 'event',
    //          eventCategory: checkBoxCategory,
    //          eventAction: label,
    //          eventLabel: label
    //       });

    // });
    
    // $('.gaContact').click(function(e){
    //         // google analytics click functionality
    //       var label = $(this).attr('data-label');
    //       var test = ga('send', {
    //          hitType: 'event',
    //           eventCategory: checkBoxCategory,
    //          eventAction: 'Contact',
    //          eventLabel: 'Contact'
    //       });

    // });
    
    $(function(){
        $('#email_res').on('click',function(){
           $('#res_ids').val(resource_ids);
        });
    });
    
    //   $(document).on('click', '.viewPrint', function(event) {
    //     console.log("view/print");
    //      window.open('http://google.com');
    //         });
        
    // });
</script>
@endsection

