<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <!--   <div class="model-close" data-dismiss="modal">
          <img src="{{asset('img/close-white.png')}}" alt="img" />  <span aria-hidden="true">×</span>
    </div> -->
        <div class="default-box">
            <div class="modal-header">
                <h5 class="modal-title text-center">Fill out the information below to send an email containing this
                    tool.</h5>
                <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-validate" method="POST" action="{{route('sendEmails')}}" id="email-form">
                    @csrf
                    <input type="hidden" id="res_ids" value="" name="res_ids">
                    <div class="form-group">
                        <label>From:</label>
                      
                        <input id="email-from" type="email" class="form-control" placeholder="Please enter your email" value="support@pghdsupport.com" readonly="readonly" name="fromEmail">
                        <div class="invalid-feedback">Please enter your email</div>
                    </div>
                    <div class="form-group">
                        <label>To:</label>
                        <input id="email-to" type="email" class="form-control" placeholder="Please enter your recipient's email" required  name="toEmail" />
                        <div class="invalid-feedback">Please enter your recipient's email</div>
                    </div>
                    <div class="form-group">
                        <label>Subject (not editable):</label>
                        <div id="email-subject" class="form-control" disabled=""><p id="ebres1">A resource has been shared with you</p><p id="ebresmul">Multiple resources have been shared with you</p></div>
                        <input id="email-subject-hidden" type="hidden" class="form-control" readonly="readonly" name="emailSubject" value="Multiple resources has been shared with you">
                    </div>
                    <div class="form-group">
                        <label>Body (not editable):</label>
                        <div id="email-body" class="form-control" disabled="">
                            <br></div>
                        <input id="email-body-hidden" class="form-control" type="hidden" value="" name="emailBody">
                    </div>
                    <div class="form-action text-right">
                        <button type="submit" class="model-btn" id="send-email">Send Message</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>